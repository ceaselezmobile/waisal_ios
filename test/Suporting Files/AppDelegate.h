//
//  AppDelegate.h
//  test
//
//  Created by ceaselez on 27/11/17.
//  Copyright © 2017 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>
@import Firebase;

@interface AppDelegate : UIResponder <UIApplicationDelegate, FIRMessagingDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

