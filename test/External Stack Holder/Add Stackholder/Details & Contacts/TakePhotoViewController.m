//
//  TakePhotoViewController.m
//  test
//
//  Created by ceaselez on 12/12/17.
//  Copyright © 2017 ceaselez. All rights reserved.
//

#import "TakePhotoViewController.h"
#import <AssetsLibrary/ALAsset.h>
#import <AssetsLibrary/ALAssetRepresentation.h>
#import "MySharedManager.h"

@interface TakePhotoViewController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIGestureRecognizerDelegate>
{
#define FILE_EXTENSION    @".png"
#define IMAGE_NAME        @"CameraImage"
}
@end

@implementation TakePhotoViewController
{
    __weak IBOutlet UIImageView *visitngCardIV;
    __weak IBOutlet UIImageView *idIV;
    UIImagePickerController *visitingCardPicker;
    UIImagePickerController *idPicker;
    MySharedManager *sharedManager;
    __weak IBOutlet UITextField *contentTF;
    __weak IBOutlet UIView *popOverView;
    __weak IBOutlet UIImageView *popOverIV;
    
    NSURL *url1;
    NSURL *url2;
    NSString *imageString1;
    NSString *imageString2;

}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    sharedManager = [MySharedManager sharedManager];
    popOverView.hidden = YES;
    if( sharedManager.imageBase64String2){
     UIImage *image = [self decodeBase64ToImage:sharedManager.imageBase64String2];
        UIImage *image1 = [self decodeBase64ToImage:sharedManager.imageBase64String1];
        visitngCardIV.image = image1;
         idIV.image = image;
    }
    else{
        NSLog(@"come here--------");
        idIV.image = [UIImage imageNamed:@"profile.png"];
        visitngCardIV.image = [UIImage imageNamed:@"profile.png"];

    }
    idIV.userInteractionEnabled = YES;
    popOverIV.userInteractionEnabled = NO;

    UITapGestureRecognizer *tapGesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapGesture:)];
    tapGesture1.numberOfTapsRequired = 1;
    [tapGesture1 setDelegate:self];
    [idIV addGestureRecognizer:tapGesture1];
    visitngCardIV.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGesture2 = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapGesture1:)];
    tapGesture2.numberOfTapsRequired = 1;
    [tapGesture2 setDelegate:self];
    [visitngCardIV addGestureRecognizer:tapGesture2];
    popOverView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGesture3 = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapGesture2:)];
    tapGesture3.numberOfTapsRequired = 1;
    [tapGesture3 setDelegate:self];
    [popOverView addGestureRecognizer:tapGesture3];
    visitingCardPicker = [[UIImagePickerController alloc] init];
    visitingCardPicker.delegate = self;
    visitingCardPicker.allowsEditing = YES;
    idPicker = [[UIImagePickerController alloc] init];
    idPicker.delegate = self;
    idPicker.allowsEditing = YES;
}
- (void) tapGesture: (id)sender
{
    popOverView.hidden = NO;
    popOverIV.image = idIV.image;
}
- (void) tapGesture1: (id)sender
{
    popOverView.hidden = NO;
    popOverIV.image = visitngCardIV.image;
}
- (void) tapGesture2: (id)sender
{
    popOverView.hidden = YES;
}
- (IBAction)visitingCardBtn:(id)sender {
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Select" message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"Take a photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        visitingCardPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:visitingCardPicker animated:YES completion:nil];
    }];
    
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"Choose from Libary" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        visitingCardPicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        [self presentViewController:visitingCardPicker animated:YES completion:nil];

    }];
    
    [actionSheet addAction:action1];
    [actionSheet addAction:action2];
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
    }]];
    [self presentViewController:actionSheet animated:YES completion:nil];
 
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];

    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
        
        UIImage* cameraImage = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        
        ALAssetsLibrary* assetsLibrary = [[ALAssetsLibrary alloc] init];
        
        [assetsLibrary writeImageToSavedPhotosAlbum:cameraImage.CGImage
                                           metadata:[info objectForKey:UIImagePickerControllerMediaMetadata]
                                    completionBlock:^(NSURL *assetURL, NSError *error) {
                                        
                                        if (!error) {
                                            if (picker == visitingCardPicker) {
                                                visitngCardIV.image = chosenImage;
                                                url1 = assetURL;
                                                imageString1 = [self encodeToBase64String:chosenImage];
                                            }
                                            else{
                                                idIV.image = chosenImage;
                                                url2 = assetURL;
                                                imageString2 = [self encodeToBase64String:chosenImage];
                                                sharedManager.imageBase64String2 = imageString2;
                                                NSLog(@"imageString2---------%@",imageString2);
                                            }

                                        }
                                    }];
    }
else
{
    if (picker == visitingCardPicker) {
        visitngCardIV.image = chosenImage;
        url1 = [info objectForKey:@"UIImagePickerControllerReferenceURL"];
        NSLog(@"url1------%@",url1);
        imageString1 = [self encodeToBase64String:chosenImage];
    }
    else{
        idIV.image = chosenImage;
        url2 = [info objectForKey:@"UIImagePickerControllerReferenceURL"];
        imageString2 = [self encodeToBase64String:chosenImage];
        sharedManager.imageBase64String2 = imageString2;
        NSLog(@"imageString2---------%@",imageString2);


    }
}
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}
- (IBAction)profile:(id)sender {
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Select" message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"Take a photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        idPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:idPicker animated:YES completion:nil];
    }];
    
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"Choose from Libary" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        idPicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        [self presentViewController:idPicker animated:YES completion:nil];
        
    }];
    
    [actionSheet addAction:action1];
    [actionSheet addAction:action2];
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
    }]];
    [self presentViewController:actionSheet animated:YES completion:nil];
  
}
- (IBAction)backBtn:(id)sender {
    if (sharedManager.imageString1  == nil) {
        sharedManager.imageString1 = @"";
    }
    else
    sharedManager.imageString1 =sharedManager.imageString1;
    if (sharedManager.imageString2 == nil) {
        sharedManager.imageString2 = @"";
    }
    else
    sharedManager.imageString2 =sharedManager.imageString2 ;
    if (sharedManager.imageBase64String1 == nil) {
        sharedManager.imageBase64String1 = @"";
    }
    else
    sharedManager.imageBase64String1 = sharedManager.imageBase64String1;
    if (sharedManager.imageBase64String2 == nil) {
        sharedManager.imageBase64String2 = @"";
    }
    else
    sharedManager.imageBase64String2 = sharedManager.imageBase64String2;
    [self dismissViewControllerAnimated:YES completion:nil];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)uploadBtn:(id)sender {
    if([[NSString stringWithFormat:@"%@",url1] isEqualToString:@""]||[[NSString stringWithFormat:@"%@",url1] isEqual:[NSNull null]]||url1.absoluteString.length==0)
    {
        imageString1=sharedManager.imageBase64String1;
    }
    else
    {
        sharedManager.imageString1 =[NSString stringWithFormat:@"%@",url1];
    }
    if([[NSString stringWithFormat:@"%@",url2] isEqualToString:@""]||[[NSString stringWithFormat:@"%@",url2] isEqual:[NSNull null]]||url2.absoluteString.length==0)
    {
        imageString2=sharedManager.imageBase64String2;
    }
    else
    {
        sharedManager.imageString2 =[NSString stringWithFormat:@"%@",url2] ;
    }
    sharedManager.passingString = contentTF.text;
    sharedManager.imageBase64String1 = imageString1;
    sharedManager.imageBase64String2=imageString2;
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (NSString *)encodeToBase64String:(UIImage *)image {
    NSData *imgData= UIImageJPEGRepresentation(image,1.0 /*compressionQuality*/);
    image=[UIImage imageWithData:imgData];
    image = [self imageWithImage:image scaledToSize:CGSizeMake(500, 500)];
    NSLog(@"string-----%@",[UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength]);
    return [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}
- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}
- (UIImage *)decodeBase64ToImage:(NSString *)strEncodeData {
    NSData *data = [[NSData alloc]initWithBase64EncodedString:strEncodeData options:NSDataBase64DecodingIgnoreUnknownCharacters];
    return [UIImage imageWithData:data];
}
@end
