

#import "AboutVC.h"
#import "MBProgressHUD.h"


@interface AboutVC ()

@end

@implementation AboutVC

- (void)viewDidLoad {
    [super viewDidLoad];
    NSString *htmlFile;
    self.headertxtlbl.text=self.str;
    _webView = [[WKWebView alloc] initWithFrame:self.view.frame];
    _webView.navigationDelegate = self;
    _webView.frame = CGRectMake(self.view.frame.origin.x,self.view.frame.origin.y+64, self.view.frame.size.width, self.view.frame.size.height-64);
    [self.view addSubview:_webView];
    if([self.str isEqualToString:@"About Us"])
    {
        htmlFile = [[NSBundle mainBundle] pathForResource:@"about" ofType:@"html"];
    }
    else if ([self.str isEqualToString:@"Privacy Policy"])
    {
        htmlFile = [[NSBundle mainBundle] pathForResource:@"PrivacyandPolicy" ofType:@"html"];
    }
    else if ([self.str isEqualToString:@"Terms & Conditions"])
    {
        htmlFile = [[NSBundle mainBundle] pathForResource:@"TandC" ofType:@"html"];
    }
    NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
    [self showProgress];
    [_webView loadHTMLString:htmlString baseURL: [[NSBundle mainBundle] bundleURL]];
    
    
}
-(void)viewWillAppear:(BOOL)animated
{
    //    [self hideProgress];
}
- (void)webView:(WKWebView *)webView
didFinishNavigation:(WKNavigation *)navigation
{
    [self hideProgress];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (IBAction)backbtn:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)showProgress
{
    [self hideProgress];
    [MBProgressHUD showHUDAddedTo:_webView animated:YES];
}

-(void)hideProgress
{
    [MBProgressHUD hideHUDForView:_webView animated:YES];
}
@end
