//
//  InvoiceDetailVC.m
//  test
//
//  Created by ceaselez on 24/07/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import "InvoiceDetailVC.h"
#import "MyMeetingTableViewCell.h"
#import "MySharedManager.h"
#import "Utitlity.h"
#import "Reachability.h"
#import "WebServices.h"
#import "GlobalURL.h"
#import "MBProgressHUD.h"
#import "ReceiptTableViewCell.h"
@interface InvoiceDetailVC ()
@property (weak, nonatomic) IBOutlet UITableView *ReceiptTableView;
@property (weak, nonatomic) IBOutlet UITableView *InvoiceTableView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@end

@implementation InvoiceDetailVC
{
    NSMutableArray *dataArray;
    long tableIndex;
    int textFieldNO;
    long lenght;
    int range;
    NSArray *headerArray;
    NSArray *contentArray;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    dataArray = [[NSMutableArray alloc] init];
    headerArray = [NSArray arrayWithObjects:@"Category :",@"Company :",@"Contact :",@"GSTIN :",@"Fiscal Year :",@"Invoice No :",@"Invoice Date :",@"Due Date :",@"Taxable Amount :",@"Total Tax :",@"Total Amount :",@"Approver :",@"Remarks :", nil];
    lenght = 0;
    range = 0;
    _scrollView.hidden = YES;
    _headerLabel.text  = @"Invoice Details";
    [self apiCall];
}
-(void)viewWillAppear:(BOOL)animated{
    [self viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [self apiCall];
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if ( tableView == _ReceiptTableView) {
        return headerArray.count+1;
    }
    else{
        if (dataArray.count == 0) {
            return  0;
        }
        if ([[[dataArray objectAtIndex:0] objectForKey:@"lstServiceInvoiceItem"] count] == 0) {
            return 1;
        }
        return [[[dataArray objectAtIndex:0] objectForKey:@"lstServiceInvoiceItem"] count];
    }
    
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView == _ReceiptTableView) {
        ReceiptTableViewCell *cell;
        if(indexPath.row == 8){
            cell = [tableView dequeueReusableCellWithIdentifier:@"status"];
            [cell.statusBtn setTitle:[[dataArray objectAtIndex:0] objectForKey:@"Status"] forState:UIControlStateNormal];
        }
        else{

            cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
            cell.detailContentTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);

            if (indexPath.row > 8) {
                cell.detailTopicLabel.text = [headerArray objectAtIndex:indexPath.row-1];
                cell.detailContentTF.text = [contentArray objectAtIndex:indexPath.row-1];
            }
            else{
            cell.detailTopicLabel.text = [headerArray objectAtIndex:indexPath.row];
            cell.detailContentTF.text = [contentArray objectAtIndex:indexPath.row];
            }
        }
       
        return  cell;
    }
    else{
        if ([[[dataArray objectAtIndex:0] objectForKey:@"lstServiceInvoiceItem"] count] == 0) {
            UITableViewCell *cell1 = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SimpleTableItem"];
            cell1.textLabel.text = @"Receiopts are not found";
            cell1.textLabel.textAlignment = NSTextAlignmentCenter;
            cell1.backgroundColor = [UIColor clearColor];
            cell1.textLabel.textColor = [UIColor blackColor];
            cell1.userInteractionEnabled  = NO;
            return cell1;
        }
        else{
            static NSString *simpleTableIdentifier = @"dataCell";
            MyMeetingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
            cell.data1.text = [NSString stringWithFormat:@"%@",[[[[dataArray objectAtIndex:0] objectForKey:@"lstServiceInvoiceItem"] objectAtIndex:indexPath.row] objectForKey:@"ServiceDate"]];
            cell.data2.text = [NSString stringWithFormat:@"%@",[[[[dataArray objectAtIndex:0] objectForKey:@"lstServiceInvoiceItem"] objectAtIndex:indexPath.row] objectForKey:@"ServiceName"]];
            cell.data3.text = [NSString stringWithFormat:@"%@",[[[[dataArray objectAtIndex:0] objectForKey:@"lstServiceInvoiceItem"] objectAtIndex:indexPath.row] objectForKey:@"SACCode"]];
            cell.data4.text = [Utitlity currencyFormate:[[[[[dataArray objectAtIndex:0] objectForKey:@"lstServiceInvoiceItem"] objectAtIndex:indexPath.row] objectForKey:@"ScheduledAmount"] intValue]];
            cell.data5.text = [Utitlity currencyFormate:[[[[[dataArray objectAtIndex:0] objectForKey:@"lstServiceInvoiceItem"] objectAtIndex:indexPath.row] objectForKey:@"CumulativeAmount"]intValue]];
            cell.data6.text = [Utitlity currencyFormate:[[[[[dataArray objectAtIndex:0] objectForKey:@"lstServiceInvoiceItem"] objectAtIndex:indexPath.row] objectForKey:@"RemainingAmount"] intValue]];
            cell.data7.text = [Utitlity currencyFormate:[[[[[dataArray objectAtIndex:0] objectForKey:@"lstServiceInvoiceItem"] objectAtIndex:indexPath.row] objectForKey:@"Amount"] intValue]];
            cell.data8.text = [NSString stringWithFormat:@"%@",[[[[dataArray objectAtIndex:0] objectForKey:@"lstServiceInvoiceItem"] objectAtIndex:indexPath.row] objectForKey:@"Description"]];
            return  cell;
        }
    }
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    static NSString *simpleTableIdentifier = @"headerCell";
    MyMeetingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    cell.header1.text = @"Schedule Date";
    cell.header2.text = @"Service Name";
    cell.header3.text = @"SAC Code";
    cell.header4.text = @"Amount(Scheduled)";
    cell.header5.text = @"Amount(Cumulative)";
    cell.header6.text = @"Amount(Remaining)";
    cell.header7.text = @"Enter Amount";
    cell.header8.text = @"Description";
    return  cell;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (dataArray.count != 0 && tableView == _InvoiceTableView) {
        return 44;
    }
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _ReceiptTableView) {
        return 80;
    }
    if ([[[dataArray objectAtIndex:0] objectForKey:@"lstServiceInvoiceItem"] count] == 0) {
        return  self.view.bounds.size.height - 100;
    }
    return 44;
}
-(void)apiCall{
    if([Utitlity isConnectedTointernet]){
        
        [self showProgress];
        NSMutableURLRequest *urlRequest;
        urlRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@GetInvoiceDetails?serviceinvoiceid=%@",BASEURL,_ServiceInvoiceID]]];
        
        //create the Method "GET"
        [urlRequest setHTTPMethod:@"GET"];
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                          {
                                              NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                                              if(httpResponse.statusCode == 200)
                                              {
                                                  NSError *parseError = nil;
                                                  dataArray = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
                                                  NSLog(@"urlRequest-----%@",dataArray);
                                                  
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      [self hideProgress];

                                                      contentArray = [NSArray arrayWithObjects:[[dataArray objectAtIndex:0] objectForKey:@"Category"],
                                                                      [[dataArray objectAtIndex:0] objectForKey:@"Company"],
                                                                      [[dataArray objectAtIndex:0] objectForKey:@"ContactName"],
                                                                      [[dataArray objectAtIndex:0] objectForKey:@"GSTNo"],
                                                                      [NSString stringWithFormat:@"%@",[[dataArray objectAtIndex:0] objectForKey:@"FiscalYearName"]],
                                                                      [[dataArray objectAtIndex:0] objectForKey:@"InvoiceNo"],
                                                                      [[dataArray objectAtIndex:0] objectForKey:@"InvoiceDate"],
                                                                      [[dataArray objectAtIndex:0] objectForKey:@"DueDate"],
                                                                      [Utitlity currencyFormate:[[[dataArray objectAtIndex:0] objectForKey:@"TaxableAmount"] intValue]],
                                                                      [Utitlity currencyFormate:[[[dataArray objectAtIndex:0] objectForKey:@"TotalTaxAmount"] intValue]],
                                                                      [Utitlity currencyFormate:[[[dataArray objectAtIndex:0] objectForKey:@"TotalAmount"] intValue]],
                                                                      [[dataArray objectAtIndex:0] objectForKey:@"ApproverName"],
                                                                      [[dataArray objectAtIndex:0] objectForKey:@"Remarks"], nil];
                                                      [_ReceiptTableView reloadData];
                                                      [_InvoiceTableView reloadData];
                                                  });
                                              }
                                              else
                                              {
                                                  NSLog(@"Error");
                                              }
                                          }];
        [dataTask resume];
    }else{
        [self showMsgAlert:NOInternetMessage];
    }
    
}
-(void)showProgress{
    [self hideProgress];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
}

-(void)hideProgress{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}


-(void)showMsgAlert:(NSString *)msg{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:nil
                                                                  message:msg
                                                           preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:ok];
    
    
    [self presentViewController:alert animated:YES completion:nil];
}
- (IBAction)receiptButton:(id)sender {
    _headerLabel.text  = @"Invoice Details";
    _scrollView.hidden = YES;
}
- (IBAction)invoiceButton:(id)sender {
    _headerLabel.text  = @"Invoice Service";
    _scrollView.hidden = NO;
}
- (IBAction)backBtn:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
