//
//  InvoiceClosureApprovalVC.m
//  test
//
//  Created by ceaselez on 20/07/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import "InvoiceClosureApprovalVC.h"
#import "Utitlity.h"
#import "Reachability.h"
#import "WebServices.h"
#import "GlobalURL.h"
#import "MBProgressHUD.h"
#import "SWRevealViewController.h"
#import "MySharedManager.h"
#import "AlertTableViewCell.h"
#import "SearchViewController.h"
@interface InvoiceClosureApprovalVC ()
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@property (weak, nonatomic) IBOutlet UIButton *headerBtn1;
@property (weak, nonatomic) IBOutlet UIButton *headerBtn2;
@property (weak, nonatomic) IBOutlet UIButton *headerBtn3;
@property (weak, nonatomic) IBOutlet UIView *headerBtn1View;
@property (weak, nonatomic) IBOutlet UIView *headerBtn2View;
@property (weak, nonatomic) IBOutlet UIView *headerBtn3View;
@property (weak, nonatomic) IBOutlet UITableView *dataTableView;
@property (weak, nonatomic) IBOutlet UIButton *menuBtn;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UIView *popOverView;
@property (weak, nonatomic) IBOutlet UIButton *fiscalYearBtn;
@property (nonatomic, strong) NSArray *searchResult;
@property (weak, nonatomic) IBOutlet UIView *searchView;
@property (weak, nonatomic) IBOutlet UITableView *popOverTableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *closeBtnLeadingConstraint;
@end

@implementation InvoiceClosureApprovalVC
{
    NSArray *invoiceList;
    UIRefreshControl *refreshControl;
    MySharedManager *sharedManager;
    BOOL searchEnabled;
    NSString *dueString;
    NSString *fiscalId;
    NSInteger selectedIndex;
    NSArray *docsList;
    NSString *textViewText;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    sharedManager = [MySharedManager sharedManager];
    invoiceList = [[NSArray alloc] init];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    
    if ( revealViewController )
    {
        [_menuBtn addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    refreshControl = [[UIRefreshControl alloc]init];
    [self.dataTableView addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(loadDataFromApi) forControlEvents:UIControlEventValueChanged];
    
}
-(void) viewWillAppear:(BOOL)animated{
    dueString = @"pending";
    _headerLabel.text  = @"Pending Invoice List";
    _closeBtnLeadingConstraint.constant = 220;
    _headerBtn1View.hidden = NO;
    _headerBtn2View.hidden = YES;
    _headerBtn3View.hidden = YES;
    _popOverView.hidden = YES;
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [self searchBarCancelButtonClicked:_searchBar];
    if ([sharedManager.slideMenuSlected isEqualToString:@"yes"]) {
        fiscalId = @"NA";
        [_fiscalYearBtn setTitle:@"----Select----" forState:UIControlStateNormal];

    }
    else{
        [self fillData];
    }
    [self loadDataFromApi];
    _popOverView.hidden = YES;
}
-(void)viewDidAppear:(BOOL)animated{
    if ([sharedManager.slideMenuSlected isEqualToString:@"yes"]) {
        sharedManager.slideMenuSlected = @"no";
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView == _popOverTableView) {
        return  3;
    }
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == _popOverTableView) {
        if (section == 0) {
            return 1;
        }
        else if (section == 1) {
            if([[[docsList objectAtIndex:0] objectForKey:@"lstreceipt"] count] == 0){
                return 1;
            }
            return [[[docsList objectAtIndex:0] objectForKey:@"lstreceipt"] count];
        }
        else {
            return 4;
        }
    }
    if (invoiceList.count == 0) {
        return 1;
    }
    if (searchEnabled) {
        if (_searchResult.count == 0) {
            return 1;
        }
        return [self.searchResult count];
    }
    else{
        return invoiceList.count;
    }
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if ( section == 0 ) {
        return @"Details";
    }
    if ( section == 1 ) {
        return @"Recipts";
    }
    return @"";
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AlertTableViewCell *cell;
    if (tableView == _dataTableView) {
        if (invoiceList.count == 0 || (searchEnabled && _searchResult.count == 0)) {
            UITableViewCell *cell1 = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SimpleTableItem"];
            cell1.textLabel.text = @"Data not Found";
            cell1.textLabel.textAlignment = NSTextAlignmentCenter;
            cell1.backgroundColor = [UIColor clearColor];
            cell1.textLabel.textColor = [UIColor blackColor];
            cell1.userInteractionEnabled = NO;
            return cell1;
        }
        else if (searchEnabled) {
                cell= [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
                cell.closeCompany.text = [[_searchResult objectAtIndex: indexPath.row] objectForKey:@"CompanyName"];
                cell.colseInvoiceNumber.text = [[_searchResult objectAtIndex: indexPath.row] objectForKey:@"InvoiceNo"];
                cell.closeInoviceTotalAmount.text = [Utitlity currencyFormate:[[[_searchResult objectAtIndex: indexPath.row] objectForKey:@"TotalAmount"] intValue]];
            cell.receivedamountLabel.text = [Utitlity currencyFormate:[[[_searchResult objectAtIndex: indexPath.row] objectForKey:@"ReceivedAmount"] intValue]];
            cell.invoiceStatusLabel.text = [[_searchResult objectAtIndex: indexPath.row] objectForKey:@"ApprovalStatus"];
             cell.invoiceDateLabel.text = [[_searchResult objectAtIndex: indexPath.row] objectForKey:@"InvoiceDate"];
        }
        else{
            cell= [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
            cell.closeCompany.text = [[invoiceList objectAtIndex: indexPath.row] objectForKey:@"CompanyName"];
            cell.colseInvoiceNumber.text = [[invoiceList objectAtIndex: indexPath.row] objectForKey:@"InvoiceNo"];
            cell.closeInoviceTotalAmount.text = [Utitlity currencyFormate:[[[invoiceList objectAtIndex: indexPath.row] objectForKey:@"TotalAmount"] intValue]];
            cell.receivedamountLabel.text = [Utitlity currencyFormate:[[[invoiceList objectAtIndex: indexPath.row] objectForKey:@"ReceivedAmount"] intValue]];
            cell.invoiceStatusLabel.text = [[invoiceList objectAtIndex: indexPath.row] objectForKey:@"ApprovalStatus"];
            cell.invoiceDateLabel.text = [[invoiceList objectAtIndex: indexPath.row] objectForKey:@"InvoiceDate"];
        }

    }
    else{
        if ( indexPath.section == 0) {
            cell= [tableView dequeueReusableCellWithIdentifier:@"Details" forIndexPath:indexPath];

            cell.CloseInvoiceNoLabel.text = [NSString stringWithFormat:@"   %@",[[docsList objectAtIndex: 0] objectForKey:@"InvoiceNo"]];
            cell.closeInvoiceDateLabel.text = [NSString stringWithFormat:@"   %@",[[docsList objectAtIndex: 0] objectForKey:@"InvoiceDate"]];
            cell.closeDueDateLabel.text = [NSString stringWithFormat:@"   %@",[[docsList objectAtIndex: 0] objectForKey:@"DueDate"]];
            cell.closeGSTINLabel.text = [NSString stringWithFormat:@"   %@",[[docsList objectAtIndex: 0] objectForKey:@"GSTIN"]];
            cell.closeTaxableAmountLabel.text = [NSString stringWithFormat:@"   %@",[Utitlity currencyFormate:[[[docsList objectAtIndex: 0] objectForKey:@"TaxableAmount"] intValue]]];
            cell.closeTotalTaxLabel.text = [NSString stringWithFormat:@"   %@",[Utitlity currencyFormate:[[[docsList objectAtIndex: 0] objectForKey:@"TotalTax"] intValue]]];
            cell.closeTotalAmountLabel.text = [NSString stringWithFormat:@"   %@",[Utitlity currencyFormate:[[[docsList objectAtIndex: 0] objectForKey:@"TotalAmount"] intValue]]];
        }
        else if ( indexPath.section == 1) {
            if ([[[docsList objectAtIndex: 0] objectForKey:@"lstreceipt"] count] == 0) {
                cell= [tableView dequeueReusableCellWithIdentifier:@"NoReciepts" forIndexPath:indexPath];
            }
            else{
            cell= [tableView dequeueReusableCellWithIdentifier:@"Reciepts" forIndexPath:indexPath];

            cell.closeReceiptNo.text = [NSString stringWithFormat:@"   %@",[[[[docsList objectAtIndex: 0] objectForKey:@"lstreceipt"] objectAtIndex:indexPath.row] objectForKey:@"ReceiptNo"]];
            cell.closeReceiptDateLabel.text = [NSString stringWithFormat:@"   %@",[[[[docsList objectAtIndex: 0] objectForKey:@"lstreceipt"] objectAtIndex:indexPath.row] objectForKey:@"ReceiptDate"]];
            cell.receiptTaxaleAmount.text = [NSString stringWithFormat:@"   %@",[Utitlity currencyFormate:[[[[[docsList objectAtIndex: 0] objectForKey:@"lstreceipt"] objectAtIndex:indexPath.row] objectForKey:@"TaxableAmount"] intValue]]];
            cell.closeTDSAmountLabeel.text = [NSString stringWithFormat:@"   %@",[Utitlity currencyFormate:[[[[[docsList objectAtIndex: 0] objectForKey:@"lstreceipt"] objectAtIndex:indexPath.row] objectForKey:@"TDSAmount"] intValue]]];
            cell.receiptTotalTaxLabel.text = [NSString stringWithFormat:@"   %@",[Utitlity currencyFormate:[[[[[docsList objectAtIndex: 0] objectForKey:@"lstreceipt"] objectAtIndex:indexPath.row] objectForKey:@"TotalTaxAmount"] intValue]]];
            cell.receiptTotalAmount.text = [NSString stringWithFormat:@"   %@",[Utitlity currencyFormate:[[[[[docsList objectAtIndex: 0] objectForKey:@"lstreceipt"] objectAtIndex:indexPath.row] objectForKey:@"TotalAmount"] intValue]]];
            }
        }
        else if ( indexPath.section == 2) {
            if (indexPath.row == 0) {
                cell= [tableView dequeueReusableCellWithIdentifier:@"Amount" forIndexPath:indexPath];
                cell.closeInvoiceAmount.text = [Utitlity currencyFormate:[[[docsList objectAtIndex: 0] objectForKey:@"InvoiceAmount"] intValue]];
                cell.closeReceivedAmountLabel.text = [Utitlity currencyFormate:[[[docsList objectAtIndex: 0] objectForKey:@"ReceivedAmount"] intValue]];
                cell.closeBalanceLabel.text = [Utitlity currencyFormate:[[[docsList objectAtIndex: 0] objectForKey:@"Balance"] intValue]];
            }
            if (indexPath.row == 1) {
                cell= [tableView dequeueReusableCellWithIdentifier:@"CloseDate" forIndexPath:indexPath];
                cell.CloseCloseDate.text = [[docsList objectAtIndex: 0] objectForKey:@"ClosedDate"];
            }
            if (indexPath.row == 2) {
                cell= [tableView dequeueReusableCellWithIdentifier:@"Remarks" forIndexPath:indexPath];
                cell.closeRamarkLabel.text = [[docsList objectAtIndex: 0] objectForKey:@"Remarks"];
            }
            if (indexPath.row == 3) {
                cell= [tableView dequeueReusableCellWithIdentifier:@"Comments" forIndexPath:indexPath];
                cell.closeCommentsTV.text = textViewText;
                if ( [dueString isEqualToString: @"pending"]) {
                    cell.userInteractionEnabled = YES;
                }
                else{
                    cell.userInteractionEnabled = NO;

                }
            }
        }
    }
   
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( tableView == _popOverTableView) {
        if (indexPath.section == 0) {
            return 200;
        }
        else if (indexPath.section == 1) {
            if ([[[docsList objectAtIndex: 0] objectForKey:@"lstreceipt"] count] == 0) {
                return 44;
            }
            return 180;
        }
        else if (indexPath.section == 2) {
            if (indexPath.row == 0) {
                return 100;
            }
            if (indexPath.row == 3) {
                return 80;
            }
        }
        return 60;
    }
    if (invoiceList.count == 0 ||(searchEnabled && _searchResult.count == 0)) {
        return  self.view.bounds.size.height-100;
    }
    return 182;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView == _popOverTableView && (section == 0 || 1)) {
        return 44;
    }
    return 0;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([Utitlity isConnectedTointernet]){
        selectedIndex = indexPath.row;
        [self loadDataFromApiForDocs:indexPath.row];
        
    }else{
        [self showMsgAlert:NOInternetMessage];
    }
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    NSString *newString = [textView.text stringByReplacingCharactersInRange:range withString:text];
        textViewText = newString;
    return YES;
}


-(CGFloat)heightForText:(NSString*)text withFont:(UIFont *)font andWidth:(CGFloat)width
{
    CGSize constrainedSize = CGSizeMake(width, MAXFLOAT);
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName,nil];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:text attributes:attributesDictionary];
    CGRect requiredHeight = [string boundingRectWithSize:constrainedSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    if (requiredHeight.size.width > width) {
        requiredHeight = CGRectMake(0,0,width, requiredHeight.size.height);
    }
    return requiredHeight.size.height;
}

- (void)textViewDidChange:(UITextView *)textView
{
    CGFloat fixedWidth = textView.frame.size.width;
    CGSize newSize = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = textView.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
    [_popOverTableView beginUpdates];
    textView.frame = newFrame;
    [_popOverTableView endUpdates];
  
}
-(void)loadDataFromApi{
    if([Utitlity isConnectedTointernet]){
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [self showProgress];
        NSString *targetUrl = [NSString stringWithFormat:@"%@GetInvoiceClosureList?FiscalYearID=%@&UserID=%@&Status=%@", BASEURL,fiscalId,[defaults objectForKey:@"UserID"],dueString];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setHTTPMethod:@"GET"];
        [request setURL:[NSURL URLWithString:targetUrl]];
        
        [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:
          ^(NSData * _Nullable data,
            NSURLResponse * _Nullable response,
            NSError * _Nullable error) {
              NSLog(@"targetUrl-----%@",targetUrl);
              
              invoiceList = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
              NSLog(@"%@",invoiceList);
              
              dispatch_async(dispatch_get_main_queue(), ^{
                  [self hideProgress];
                  [refreshControl endRefreshing];
                  [_dataTableView reloadData];
                  
              });
              
          }] resume];
        
        
    }else{
        [self showMsgAlert:NOInternetMessage];
    }
}
-(void)showProgress{
    [MBProgressHUD showHUDAddedTo:_dataTableView animated:YES];
    
}

-(void)hideProgress{
    [MBProgressHUD hideHUDForView:_dataTableView animated:YES];
}
-(void)showMsgAlert:(NSString *)msg{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:nil
                                                                  message:msg
                                                           preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:ok];
    
    
    [self presentViewController:alert animated:YES completion:nil];
}
- (IBAction)search:(id)sender {
    _searchView.hidden = NO;
    [_searchBar  becomeFirstResponder];
}
- (IBAction)searchBackBtn:(id)sender {
    [self searchBarCancelButtonClicked:_searchBar];
}
- (void)filterContentForSearchText:(NSString*)searchText
{
    _searchResult = [invoiceList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(InvoiceNo contains[c] %@) OR (CompanyName contains[c] %@)", searchText, searchText]];
    [_dataTableView reloadData];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    NSCharacterSet *set = [NSCharacterSet whitespaceCharacterSet];
    if ([[searchBar.text stringByTrimmingCharactersInSet: set] length] == 0) {
        searchEnabled = NO;
        [_dataTableView reloadData];
    }
    else {
        searchEnabled = YES;
        [self filterContentForSearchText:searchBar.text];
    }
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    NSCharacterSet *set = [NSCharacterSet whitespaceCharacterSet];
    if ([[searchBar.text stringByTrimmingCharactersInSet: set] length] == 0) {
        searchEnabled = NO;
        [self.dataTableView reloadData];
    }
    else {
        searchEnabled = YES;
        [self filterContentForSearchText:searchBar.text];
    }
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    [searchBar setText:@""];
    searchEnabled = NO;
    [_dataTableView reloadData];
    _searchView.hidden = YES;
    
}
- (IBAction)Rejected:(id)sender {
    dueString = @"rejected";
    _headerLabel.text  = @"Rejected Invoice List";

    _closeBtnLeadingConstraint.constant = 0;
    _headerBtn1View.hidden = YES;
    _headerBtn2View.hidden = YES;
    _headerBtn3View.hidden = NO;
    _popOverView.hidden = YES;
    [self loadDataFromApi];
}
- (IBAction)Approved:(id)sender {
    dueString = @"approved";
    _headerLabel.text  = @"Approved Invoice List";
    _closeBtnLeadingConstraint.constant = 0;
    _headerBtn1View.hidden = YES;
    _headerBtn2View.hidden = NO;
    _headerBtn3View.hidden = YES;
    _popOverView.hidden = YES;
    [self loadDataFromApi];
    
}
- (IBAction)Pending:(id)sender {
    dueString = @"pending";
    _headerLabel.text  = @"Pending Invoice List";
    _closeBtnLeadingConstraint.constant = 220;
    _headerBtn1View.hidden = NO;
    _headerBtn2View.hidden = YES;
    _headerBtn3View.hidden = YES;
    _popOverView.hidden = YES;
    [self loadDataFromApi];
}
- (IBAction)ppysicalYearBtn:(id)sender {
    if([Utitlity isConnectedTointernet]){
            sharedManager.passingMode = @"physicalYear";
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            SearchViewController *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"SearchViewController"];
            [self presentViewController:myNavController animated:YES completion:nil];
    }else{
        [self showMsgAlert:NOInternetMessage];
    }
}
-(void)fillData{
    if (sharedManager.passingString.length != 0) {
     
        if ([sharedManager.passingMode isEqualToString: @"physicalYear"]) {
            [_fiscalYearBtn setTitle:sharedManager.passingString forState:UIControlStateNormal];
            fiscalId = sharedManager.passingId;
            if ([fiscalId isEqualToString: @"00000000-0000-0000-0000-000000000000"]) {
                fiscalId = @"NA";
            }
        }
    }
}
-(void)loadDataFromApiForDocs:(NSInteger)indexSelected{
    if([Utitlity isConnectedTointernet]){
        [MBProgressHUD showHUDAddedTo:_popOverTableView animated:YES];
        NSString *targetUrl = [NSString stringWithFormat:@"%@GetClosedServiceInvoiceDetails?serviceinvoiceid=%@", BASEURL,[[invoiceList objectAtIndex:indexSelected] objectForKey:@"ServiceInvoiceID"]];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setHTTPMethod:@"GET"];
        [request setURL:[NSURL URLWithString:targetUrl]];
        
        [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:
          ^(NSData * _Nullable data,
            NSURLResponse * _Nullable response,
            NSError * _Nullable error) {
              NSLog(@"targetUrl-----%@",targetUrl);
              
              docsList = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
              NSLog(@"%@",docsList);
              
              dispatch_async(dispatch_get_main_queue(), ^{
                  [MBProgressHUD hideHUDForView:_popOverTableView animated:YES];
                  textViewText = @"";
                  _popOverView.hidden = NO;
                  [refreshControl endRefreshing];
                  [_popOverTableView reloadData];
                  
              });
              
          }] resume];
        
        
    }else{
        [self showMsgAlert:NOInternetMessage];
    }
}
- (IBAction)popOverViewOkButton:(id)sender {
    _popOverView.hidden = YES;
}
- (IBAction)rejectBtn:(id)sender {
    if([Utitlity isConnectedTointernet]){
//        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
        [params setObject:[[invoiceList objectAtIndex:selectedIndex] objectForKey:@"ServiceInvoiceID"]  forKey:@"ServiceInvoiceID"];
        [params setObject:textViewText  forKey:@"Comments"];
        [params setObject:@"NO"  forKey:@"isClosureApproved"];
        [params setObject:@"YES"  forKey:@"isClosureRejected"];
//        [params setObject:[defaults objectForKey:@"UserID"]  forKey:@"ModifiedBy"];
        
        [self showProgress];
        NSString* JsonString = [Utitlity JSONStringConv: params];
        [[WebServices sharedInstance]apiAuthwithJSON:@"POSTInvoiceClosureApproval" HTTPmethod:@"POST" forparameters:JsonString ContentType:APICONTENTTYPE apiKey:nil onCompletion:^(NSDictionary *json, NSURLResponse * headerResponse) {
            
            NSString *error=[json valueForKey:@"error"];
            [self hideProgress];
            
            if(error.length>0){
                [self showMsgAlert:[json valueForKey:@"error_description"]];
                return ;
            }else{
                [self showMsgAlert:@"Invoice Rejected successfully."];
                NSLog(@"json-----%@",json);
                _popOverView.hidden = YES;
                [self loadDataFromApi];
            }
        }];
    }else{
        [self showMsgAlert:NOInternetMessage];
    }

}
- (IBAction)approveBtn:(id)sender {
    if([Utitlity isConnectedTointernet]){
//        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
        [params setObject:[[invoiceList objectAtIndex:selectedIndex] objectForKey:@"ServiceInvoiceID"]  forKey:@"ServiceInvoiceID"];
        [params setObject:textViewText  forKey:@"Comments"];
        [params setObject:@"YES"  forKey:@"isClosureApproved"];
        [params setObject:@"NO"  forKey:@"isClosureRejected"];
        //        [params setObject:[defaults objectForKey:@"UserID"]  forKey:@"ModifiedBy"];
        
        [self showProgress];
        NSString* JsonString = [Utitlity JSONStringConv: params];
        [[WebServices sharedInstance]apiAuthwithJSON:@"POSTInvoiceClosureApproval" HTTPmethod:@"POST" forparameters:JsonString ContentType:APICONTENTTYPE apiKey:nil onCompletion:^(NSDictionary *json, NSURLResponse * headerResponse) {
            
            NSString *error=[json valueForKey:@"error"];
            [self hideProgress];
            
            if(error.length>0){
                [self showMsgAlert:[json valueForKey:@"error_description"]];
                return ;
            }else{
                [self showMsgAlert:@"Invoice Approved successfully."];
                NSLog(@"json-----%@",json);
                _popOverView.hidden = YES;
                [self loadDataFromApi];
            }
        }];
    }else{
        [self showMsgAlert:NOInternetMessage];
    }
}

@end
