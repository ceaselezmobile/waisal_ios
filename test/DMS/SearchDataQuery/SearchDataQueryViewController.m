//
//  SearchDataQueryViewController.m
//  test
//
//  Created by ceazeles on 26/12/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import "SearchDataQueryViewController.h"

@interface SearchDataQueryViewController ()

@end

@implementation SearchDataQueryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    searchArray = [NSMutableArray array];
    searchResultArray = [NSMutableArray array];
    
    noDataLabel = [[UILabel alloc]initWithFrame:CGRectMake(0,self.view.frame.size.height/2,self.view.frame.size.width ,50)];
    
    if([self.searchFlag isEqualToString:@"Category"]&&[self.searchScreen isEqualToString:@"Common"])
    {
        [self getDelegate:@"GetExternalStakeHolder"];
         self.searchBar.placeholder = @"Search by Category";
    }
    else if([self.searchFlag isEqualToString:@"Company"]&&[self.searchScreen isEqualToString:@"Common"])
    {
        [self getDelegate:[NSString stringWithFormat:@"GetMITRCompany?externalStakeholderId=%@",_theId]];
        self.searchBar.placeholder = @"Search by Company";
    }
    else if([self.searchFlag isEqualToString:@"Name"]&&[self.searchScreen isEqualToString:@"Common"])
    {
        [self getDelegate:_theURL];
        self.searchBar.placeholder = @"Search by Name";
    }
    else if([self.searchFlag isEqualToString:@"Name"]&&[self.searchScreen isEqualToString:@"External"])
    {
        [self getDelegate:_theURL];
        self.searchBar.placeholder = @"Search by Name";
    }
    
    [self.searchBar setValue:[UIColor whiteColor] forKeyPath:@"_searchField._placeholderLabel.textColor"];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(self.searchBarActive)
    {
        return searchResultArray.count;
    }
    else
    {
        return searchArray.count;
    }
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *array = [NSArray array];
    if(self.searchBarActive)
    {
        array = [searchResultArray mutableCopy];
    }
    else
    {
        array = [searchArray mutableCopy];
    }
    static NSString *cellIdentifier = @"searchCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    NSDictionary *dict = array[indexPath.row];
    if([self.searchFlag isEqualToString:@"Category"]&&[self.searchScreen isEqualToString:@"Common"])
    {
        cell.textLabel.text = dict[@"Type"];
    }
    else if([self.searchFlag isEqualToString:@"Company"]&&[self.searchScreen isEqualToString:@"Common"])
    {
        cell.textLabel.text = dict[@"CompanyName"];
    }
    else if([self.searchFlag isEqualToString:@"Name"]&&[self.searchScreen isEqualToString:@"Common"])
    {
        cell.textLabel.text = dict[@"Name"];
    }
    else if([self.searchFlag isEqualToString:@"Name"]&&[self.searchScreen isEqualToString:@"External"])
    {
        cell.textLabel.text = dict[@"ContactName"];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *array = [NSArray array];
    if(self.searchBarActive)
    {
        array = [searchResultArray mutableCopy];
    }
    else
    {
        array = [searchArray mutableCopy];
    }
    NSDictionary *dict = array[indexPath.row];
    if(self.delegate != nil)
    {
        if([self.searchFlag isEqualToString:@"Category"]&&[self.searchScreen isEqualToString:@"Common"])
        {
            [self.delegate selectedString:dict[@"Type"] selectedID:dict[@"ExternalStakeholderTypeID"] anotherId:@""  selectFlag:self.searchFlag];
        }
        else if([self.searchFlag isEqualToString:@"Company"]&&[self.searchScreen isEqualToString:@"Common"])
        {
             [self.delegate selectedString:dict[@"CompanyName"] selectedID:dict[@"IndustryId"] anotherId:dict[@"ExternalStakeholderID"] selectFlag:self.searchFlag];
        }
        else if([self.searchFlag isEqualToString:@"Name"]&&[self.searchScreen isEqualToString:@"Common"])
        {
             [self.delegate selectedString:dict[@"Name"] selectedID:dict[@"UserID"] anotherId:@"" selectFlag:self.searchFlag];
        }
        else if([self.searchFlag isEqualToString:@"Name"]&&[self.searchScreen isEqualToString:@"External"])
        {
            [self.delegate selectedString:dict[@"ContactName"] selectedID:dict[@"UserId"] anotherId:@"" selectFlag:self.searchFlag];
        }
    }
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)getDelegate :(NSString *)url
{
    if([Utitlity isConnectedTointernet]){
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[ServiceManager sharedInstance] getWithParameter:nil withUrl:[NSString stringWithFormat:@"%@",url] withHandler:^(id responseObject, NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            [self->searchArray addObjectsFromArray:responseObject];
            if(self->searchArray.count > 0)
            {
                [self removeNoDataFound:self.view];
                self.tableView.hidden = NO;
                if([self.searchFlag isEqualToString:@"Category"]&&[self.searchScreen isEqualToString:@"Common"])
                {
                    
                    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"00000000-0000-0000-0000-000000000000",@"ExternalStakeholderTypeID",@"ALL",@"Type", nil];
                    [self->searchArray insertObject:dict atIndex:0];
                }
                else if([self.searchFlag isEqualToString:@"Company"]&&[self.searchScreen isEqualToString:@"Common"])
                {
                    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"00000000-0000-0000-0000-000000000000",@"IndustryId",@"ALL",@"CompanyName", nil];
                    [self->searchArray insertObject:dict atIndex:0];
                }
                [self.tableView reloadData];
            }
            else
            {
                self.tableView.hidden = YES;
                [self showNoDataFound:self.view];
            }
            
                NSLog(@"Search Response  %@",responseObject);
            });
        }];
    }
    else{
       
        [[Utitlity sharedInstance] showAlertViewWithMessage:NOInternetMessage withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
         {
             
         }];
       
    }
}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    [searchResultArray removeAllObjects];
    NSPredicate *resultPredicate;
    if([self.searchFlag isEqualToString:@"Category"]&&[self.searchScreen isEqualToString:@"Common"])
    {
        resultPredicate = [NSPredicate predicateWithFormat:@"Type contains[c] %@", searchText];
    }
    else if([self.searchFlag isEqualToString:@"Company"]&&[self.searchScreen isEqualToString:@"Common"])
    {
       resultPredicate = [NSPredicate predicateWithFormat:@"CompanyName contains[c] %@", searchText];
    }
    else if([self.searchFlag isEqualToString:@"Name"]&&[self.searchScreen isEqualToString:@"Common"])
    {
        resultPredicate = [NSPredicate predicateWithFormat:@"Name contains[c] %@", searchText];
    }
    else if([self.searchFlag isEqualToString:@"Name"]&&[self.searchScreen isEqualToString:@"External"])
    {
        resultPredicate = [NSPredicate predicateWithFormat:@"ContactName contains[c] %@", searchText];
    }
    searchResultArray  = [NSMutableArray arrayWithArray:[searchArray filteredArrayUsingPredicate:resultPredicate]];
    if(searchResultArray.count > 0)
    {
        self.tableView.hidden = NO;
        [self.tableView reloadData];
        [self removeNoDataFound:self.view];
    }
    else{
        self.tableView.hidden = YES;
        [self showNoDataFound:self.view];
    }
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (searchText.length>0)
    {
        self.searchBarActive = YES;
        [self filterContentForSearchText:searchText scope:[[self.searchBar scopeButtonTitles] objectAtIndex:[self.searchBar selectedScopeButtonIndex]]];
        
    }else{
        [self removeNoDataFound:self.view];
        self.searchBarActive = NO;
        self.tableView.hidden = NO;
        [self.tableView reloadData];
    }
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [self cancelSearching];
    [self.tableView reloadData];
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    self.searchBarActive = YES;
    [self.view endEditing:YES];
}
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [self.searchBar setShowsCancelButton:NO animated:YES];
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    self.searchBarActive = NO;
    [self.searchBar setShowsCancelButton:NO animated:YES];
}
-(void)cancelSearching
{
    self.searchBarActive = NO;
    [self.searchBar resignFirstResponder];
    self.searchBar.text  = @"";
}

-(void)showNoDataFound :(UIView *)view
{
    if(![noDataLabel isDescendantOfView:view])
    {
        [view addSubview:noDataLabel];
    }
    [noDataLabel setFont:[UIFont systemFontOfSize:20]];
    noDataLabel.textAlignment = NSTextAlignmentCenter;
    noDataLabel.layer.masksToBounds  = YES;
    noDataLabel.layer.shadowOpacity  = 2.5;
    noDataLabel.layer.shadowColor    = [[UIColor grayColor] CGColor];
    noDataLabel.layer.shadowOffset   = CGSizeMake(0, 1);
    noDataLabel.layer.shadowRadius   = 2;
    noDataLabel.text=@"No Data Found";
}

-(void)removeNoDataFound :(UIView *)view
{
    [noDataLabel removeFromSuperview];
}


    
- (IBAction)backButtonClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


@end
