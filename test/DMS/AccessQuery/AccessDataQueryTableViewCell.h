//
//  AccessDataQueryTableViewCell.h
//  test
//
//  Created by ceazeles on 24/12/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AccessDataQueryTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *folderNameHeaderText;
@property (weak, nonatomic) IBOutlet UITextField *pathNameHeaderText;
@property (weak, nonatomic) IBOutlet UITextField *folderDateTimeHeaderText;
@property (weak, nonatomic) IBOutlet UITextField *fileHeaderText;
@property (weak, nonatomic) IBOutlet UITextField *dateTimeHeaderText;

@property (weak, nonatomic) IBOutlet UITextField *folderNameDataText;
@property (weak, nonatomic) IBOutlet UITextField *pathNameDataText;
@property (weak, nonatomic) IBOutlet UITextField *folderDateTimeDataText;
@property (weak, nonatomic) IBOutlet UITextField *fileDataText;
@property (weak, nonatomic) IBOutlet UITextField *dateTimeText;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pathHeaderWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pathDataWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *fileHeaderWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *fileDataWidth;

@end

NS_ASSUME_NONNULL_END
