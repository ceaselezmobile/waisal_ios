//
//  LoginQueryViewController.h
//  test
//
//  Created by ceazeles on 20/12/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginQueryTableViewCell.h"
#import "SearchDataQueryViewController.h"
#import "SWRevealViewController.h"
#import "MySharedManager.h"
#import "THDatePickerViewController.h"
#import "Utitlity.h"
#import "LoginQueryTableViewController.h"
#import "UITextField+PaddingText.h"


NS_ASSUME_NONNULL_BEGIN

@interface LoginQueryViewController : UIViewController
{
    MySharedManager *sharedManager;
    THDatePickerViewController * datePicker;
    NSDateFormatter * formatter;
    NSString *dateType;
    NSString *count;
    NSString *categoryId;
    NSString *companyId;
    NSString *externalStakeHolderId;
    NSString *nameId;
    UILabel *noDataLabel;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *menuButton;

@end

NS_ASSUME_NONNULL_END
