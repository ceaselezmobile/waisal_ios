//
//  LoginDataQueryTableViewCell.h
//  test
//
//  Created by ceazeles on 24/12/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LoginDataQueryTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *loginDateHeaderText;
@property (weak, nonatomic) IBOutlet UITextField *loginIpHeaderText;
@property (weak, nonatomic) IBOutlet UITextField *locationHeaderText;

@property (weak, nonatomic) IBOutlet UITextField *loginDateDataText;
@property (weak, nonatomic) IBOutlet UITextField *loginIpDataText;
@property (weak, nonatomic) IBOutlet UITextField *locationDataText;

@end

NS_ASSUME_NONNULL_END
