//
//  MyActivityQueryTableViewController.h
//  test
//
//  Created by ceazeles on 09/01/19.
//  Copyright © 2019 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyActivityQueryTableViewCell.h"
#import "MBProgressHUD.h"
#import "ServiceManager.h"
#import "GlobalURL.h"
#import "Utitlity.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyActivityQueryTableViewController : UIViewController

{
    NSMutableArray *array;
    NSMutableArray *searchArray;
    UILabel *noDataLabel;
    long lengthPath;
    long lengthFile;
    UIRefreshControl *refreshControl;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property(strong,nonatomic)NSString *theURL;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewWidth;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (assign)BOOL searchBarActive;
@end

NS_ASSUME_NONNULL_END
