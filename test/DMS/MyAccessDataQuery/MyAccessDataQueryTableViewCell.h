//
//  MyAccessDataQueryTableViewCell.h
//  test
//
//  Created by ceazeles on 08/01/19.
//  Copyright © 2019 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyAccessDataQueryTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *fromDateText;
@property (weak, nonatomic) IBOutlet UITextField *toDateText;
@end

NS_ASSUME_NONNULL_END
