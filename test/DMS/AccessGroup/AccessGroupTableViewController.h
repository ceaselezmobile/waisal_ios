//
//  AccessGroupTableViewController.h
//  test
//
//  Created by ceazeles on 27/12/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AccessGroupTableViewCell.h"
#import "MBProgressHUD.h"
#import "ServiceManager.h"
#import "GlobalURL.h"
#import "SWRevealViewController.h"
#import "Utitlity.h"

NS_ASSUME_NONNULL_BEGIN

@interface AccessGroupTableViewController : UIViewController
{
    NSMutableArray *array;
    NSMutableArray *searchResultArray;
    UILabel *noDataLabel;
    UIRefreshControl *refreshControl;
}

@property (assign)BOOL searchBarActive;
@property (weak, nonatomic) IBOutlet UIButton *menuButton;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@end

NS_ASSUME_NONNULL_END
