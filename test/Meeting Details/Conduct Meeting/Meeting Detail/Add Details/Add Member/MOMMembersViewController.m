//
//  MOMMembersViewController.m
//  test
//
//  Created by ceaselez on 24/01/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import "MOMMembersViewController.h"
#import "Utitlity.h"
#import "Reachability.h"
#import "WebServices.h"
#import "GlobalURL.h"
#import "MBProgressHUD.h"
#import "SearchViewController.h"
#import "MySharedManager.h"
#import "MOMMemberTableViewCell.h"
@interface MOMMembersViewController ()
@property (weak, nonatomic) IBOutlet UITableView *dataTableView;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *timeLabelConstain;

@end

@implementation MOMMembersViewController
{
    MySharedManager *sharedManager;

    NSArray *tittleArray;
    NSArray *contentArray;
    NSString *typeString;
    NSString *categoryString;
    NSString *CompayString;
    NSString *memberString;
    NSString *emailSting;
    NSMutableArray *emailArray;
    
    int type;
    NSString *categoryId;
    NSString *CompayId;
    NSString *memberId;
    
    NSTimer *timer;
    long currMinute;
    long currSeconds;
    long currHours;
    BOOL decrement;
    BOOL mins;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
  
    
    NSLog(@"%@",[NSString stringWithFormat:@"%02ld:%02ld:%02ld",currHours,currMinute,currSeconds]);
    [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerTick:) userInfo:nil repeats:YES];
    [self clearTF];
    sharedManager = [MySharedManager sharedManager];
    if (sharedManager.realTime) {
        decrement = sharedManager.decrement;
        currHours = sharedManager.currHour;
        currSeconds = sharedManager.currSecs;
        currMinute =  sharedManager.currmins ;
        NSString *countDownTimer=[NSString stringWithFormat:@"%02ld:%02ld:%02ld",currHours,currMinute,currSeconds];
        NSDate *now = [NSDate date];
        
        static NSDateFormatter *dateFormatter;
        if (!dateFormatter) {
            dateFormatter = [[NSDateFormatter alloc] init];
            dateFormatter.dateFormat = @"E MMM dd yyyy hh:mm:ss a";  // very simple format  "8:47:22 AM"
        }
        _timeLabel.text = [NSString stringWithFormat:@" %@ \nGMT+05:30(India Standard Time) %@",[dateFormatter stringFromDate:now],countDownTimer];
       
//        [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerTick:) userInfo:nil repeats:YES];
    }
    else{
        _timeLabel.hidden = YES;
        _timeLabelConstain.constant = 0;
    }
//    if (sharedManager.mailArray.count != 0) {
        emailArray = sharedManager.mailArray;
        typeString = @"Select";
        type = 1;
//     }
    emailArray =[NSMutableArray new];
    [self typeChanges];
}
-(void)viewWillAppear:(BOOL)animated{
    [self fillTheTF];
}
-(void)typeChanges{
    if (type == 1) {
        tittleArray = [NSArray arrayWithObjects:@"Type :",@"Category :",@"Company :",@"Member :", nil];
        contentArray = [NSArray arrayWithObjects:typeString,categoryString,CompayString,memberString, nil];
    }
    else if (type == 2){
        tittleArray = [NSArray arrayWithObjects:@"Type :",@"Email ID :", nil];
        contentArray = [NSArray arrayWithObjects:typeString,emailSting, nil];
    }
    else{
        tittleArray = [NSArray arrayWithObjects:@"Type :", nil];
        contentArray = [NSArray arrayWithObjects:@"", nil];
    }
    [_dataTableView reloadData];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (type == 1 || type ==2) {
        return tittleArray.count+emailArray.count+1;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MOMMemberTableViewCell *cell;
    if (tittleArray.count >indexPath.row) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
        cell.tittleLabel.text = [tittleArray objectAtIndex:indexPath.row];
        cell.contentTF.text = [contentArray objectAtIndex:indexPath.row];
        cell.contentTF.placeholder =[NSString stringWithFormat:@"Select %@",[tittleArray objectAtIndex:indexPath.row]];
        cell.contentTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
        if (type == 2 && indexPath.row == 1) {
            cell.cellBtn.hidden = YES;
            cell.contentTF.userInteractionEnabled = YES;
        }
        else{
            cell.cellBtn.hidden = NO;
            cell.contentTF.userInteractionEnabled = NO;
        }
    }
    else if (tittleArray.count  == indexPath.row) {
      cell = [tableView dequeueReusableCellWithIdentifier:@"button"];
    }
    else if(emailArray.count>0){
        cell = [tableView dequeueReusableCellWithIdentifier:@"email"];
        cell.textLabel.text = [emailArray objectAtIndex:indexPath.row - tittleArray.count -1];
        cell.textLabel.textColor = [UIColor blackColor];
    }
    return cell;

}
- (IBAction)cellBtn:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.dataTableView];
    NSIndexPath *indexPath = [self.dataTableView indexPathForRowAtPoint:buttonPosition];
    if (indexPath.row == 0) {
        [self  selectType];
    }
    else if (indexPath.row == 1){
        [self selectCatogry];
    }
    else if (indexPath.row == 2){
        [self selectCompany];
    }
    else if (indexPath.row == 3){
        [self selectMember];
    }
}
-(void)selectType{
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Select" message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"Member" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        typeString = @"Member";
        type = 1;
        [self typeChanges];
    }];
    
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"Not a member" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        typeString = @"Not a member";
        type = 2;
        [self typeChanges];
    }];
    
    [actionSheet addAction:action1];
    [actionSheet addAction:action2];
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
    }]];
    [self presentViewController:actionSheet animated:YES completion:nil];
}
-(void)selectCatogry{
    if([typeString isEqualToString:@"Select"])
    {
        [self showMsgAlert:@"Please Select Type"];
    }
    else
    {
    if([Utitlity isConnectedTointernet]){
        sharedManager.passingMode = @"category";
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        SearchViewController *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"SearchViewController"];
        [self presentViewController:myNavController animated:YES completion:nil];
    }else{
        [self showMsgAlert:NOInternetMessage];
    }
    }
}
-(void)selectCompany{
    if([typeString isEqualToString:@"Select"])
    {
        [self showMsgAlert:@"Please Select Type"];
    }
    else
    {
    if([Utitlity isConnectedTointernet]){
        
        if ([categoryString isEqualToString:@""]) {
            [self showMsgAlert:@"Please select Catagory"];
        }
        else{
            sharedManager.passingMode = @"company";
            sharedManager.passingId = categoryId;
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            SearchViewController *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"SearchViewController"];
            [self presentViewController:myNavController animated:YES completion:nil];
        }
    }else{
        [self showMsgAlert:NOInternetMessage];
    }
    }
}
-(void)selectMember{
    if([typeString isEqualToString:@"Select"])
    {
        [self showMsgAlert:@"Please Select Type"];
    }
    else
    {
    if([Utitlity isConnectedTointernet]){
        
        if ([CompayString isEqualToString:@""]) {
            [self showMsgAlert:@"Please select Company"];
        }
        else{
            sharedManager.passingMode = @"assignedToMOM";
            sharedManager.passingId = CompayId;
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            SearchViewController *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"SearchViewController"];
            [self presentViewController:myNavController animated:YES completion:nil];
        }
    }else{
        [self showMsgAlert:NOInternetMessage];
    }
    }
}
-(void)fillTheTF{
    if (![sharedManager.passingString isEqualToString:@""]) {
        if ([sharedManager.passingMode isEqualToString: @"category"]) {
            CompayString = @"";
            memberString = @"";
            categoryString = sharedManager.passingString;
            categoryId = sharedManager.passingId;
        }
        if ([sharedManager.passingMode isEqualToString: @"company"]) {
            memberString = @"";
            CompayString= sharedManager.passingString;
            CompayId = sharedManager.passingId;
        }
        if ([sharedManager.passingMode isEqualToString: @"assignedToMOM"]) {
            memberString= sharedManager.passingString;
            memberId = sharedManager.passingId;
        }
        [self typeChanges];

    }
}
-(void)clearTF{
    categoryString = @"";
    CompayString = @"";
    memberString = @"";
    emailSting = @"";
    sharedManager.passingString = @"";
    sharedManager.passingId = @"";
}


-(void)showMsgAlert:(NSString *)msg{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:nil
                                                                  message:msg
                                                           preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:ok];
    
    [self presentViewController:alert animated:YES completion:nil];
}
- (IBAction)doneBtn:(id)sender {
    NSIndexPath *ip = [NSIndexPath indexPathForRow:1 inSection:0];
    MOMMemberTableViewCell *cell = [_dataTableView cellForRowAtIndexPath:ip];
    if (![memberString isEqualToString:@""]) {
        [emailArray addObject:memberString];
        typeString = @"Select";
        [self clearTF];
        [self typeChanges];
        [self showMsgAlert:@"Conduct meeting Member Added successfully"];
    }
    else if(![cell.contentTF.text isEqualToString:@""] ){
        if ([self isValidEmail:cell.contentTF.text]) {
            [emailArray addObject:cell.contentTF.text];
            typeString = @"Select";
            [self clearTF];
            [self typeChanges];
            [self showMsgAlert:@"Conduct meeting Member Added successfully"];
        }
        else{
            [self showMsgAlert:@"Please enter a valid Email Id"];
        }
    }
    else{
        [self showMsgAlert:@"Please select Email Id"];
    }
}
- (IBAction)cancelBtn:(id)sender {
    sharedManager.passingString = @"";
    sharedManager.passingId = @"";
    [self clearTF];
    [self typeChanges];
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)mailIdDelete:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.dataTableView];
    NSIndexPath *indexPath = [self.dataTableView indexPathForRowAtPoint:buttonPosition];
    [emailArray removeObjectAtIndex:indexPath.row-tittleArray.count-1];
    [self typeChanges];
}
- (IBAction)backBtn:(id)sender {
    sharedManager.passingString = @"";
    sharedManager.passingId = @"";
//    [emailArray removeAllObjects];
//    sharedManager.mailArray = emailArray;
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(BOOL) isValidEmail:(NSString *)checkString
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
- (IBAction)saveBtn:(id)sender {
    sharedManager.mailArray = emailArray;
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)timerTick:(NSTimer *)timer {
    NSDate *now = [NSDate date];
    
    static NSDateFormatter *dateFormatter;
    if (!dateFormatter) {
        dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"E MMM dd yyyy hh:mm:ss a";  // very simple format  "8:47:22 AM"
    }
    NSString *countDownTimer ;
    if (decrement) {
        countDownTimer =  [self  timerFired];
    }
    else{
        countDownTimer = [self timerincrementer];
    }
    _timeLabel.text = [NSString stringWithFormat:@" %@ \nGMT+05:30(India Standard Time) %@",[dateFormatter stringFromDate:now],countDownTimer];
}
-(NSString*)timerFired
{
    if(currMinute == 0 && currSeconds == 0 && currHours == 0)
    {
        decrement = NO;
    }
    else{
        if(currMinute == 0 && currHours>0 && currSeconds==0)
        {
            currHours-=1;
            currMinute=59;
            mins = NO;
        }
        else if(currSeconds==0 && currMinute>0 )
        {
            if (mins) {
                currMinute-=1;
            }
            currSeconds=59;
        }
        else if(currSeconds>0 )
        {
            currSeconds-=1;
            mins = YES;
        }
    }
    return [NSString stringWithFormat:@"%02ld:%02ld:%02ld",currHours,currMinute,currSeconds];
}
-(NSString*)timerincrementer
{
    if(currSeconds<59)
    {
        currSeconds+=1;
    }
    else if(currSeconds == 59 && currMinute < 59)
    {
        currMinute+=1;
        currSeconds=0;
    }
    else  if(currMinute == 59)
    {
        currHours+=1;
        currMinute=0;
        currSeconds=0;
    }
    
    return [NSString stringWithFormat:@"%02ld:%02ld:%02ld",currHours,currMinute,currSeconds];
}


@end
