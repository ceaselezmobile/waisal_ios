//
//  AgendaDescriptionViewController.h
//  test
//
//  Created by ceaselez on 28/02/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AgendaDescriptionViewController : UIViewController
@property(nonatomic,strong) NSDictionary *meetingDetails;

@end
