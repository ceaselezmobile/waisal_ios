//
//  ServiceManager.m
//  D3P
//
//  Created by ceazeles on 18/07/18.
//  Copyright © 2018 ceazeles. All rights reserved.
//

#import "ServiceManager.h"


@interface ServiceManager ()

@end

@implementation ServiceManager

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

+(ServiceManager *)sharedInstance{
    
    static ServiceManager *sharedInstance = nil;
    static dispatch_once_t onceToken = 0;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[ServiceManager alloc] init];
        // Do any other initialisation stuff here
    });
    return sharedInstance;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


-(void) postwithParameter:(NSDictionary *)params  withUrl:(NSString *)url withHandler:(ResponseBlock)handler  {

    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    [manager POST:[NSString stringWithFormat:@"%@%@", BASEURL,url] parameters:params
          success:^(AFHTTPRequestOperation *operation, id responseObject){
              
              handler(responseObject,nil);
              
          }
          failure: ^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSString *ErrorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
         NSData *data1 = [ErrorResponse dataUsingEncoding:NSUTF8StringEncoding];
         id json = [NSJSONSerialization JSONObjectWithData:data1 options:0 error:nil];
         NSLog(@"Error is %@",json);
         handler(json,nil);
     }];
}

-(void) getWithParameter:(NSDictionary *)params  withUrl:(NSString *)url withHandler:(ResponseBlock)handler
{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    [manager GET:[NSString stringWithFormat:@"%@%@",BASEURL,url] parameters:params
          success:^(AFHTTPRequestOperation *operation, id responseObject){
              
              handler(responseObject,nil);
              
          }
          failure: ^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSString *ErrorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
         NSData *data1 = [ErrorResponse dataUsingEncoding:NSUTF8StringEncoding];
         id json = [NSJSONSerialization JSONObjectWithData:data1 options:0 error:nil];
         NSLog(@"Error is %@",json);
         handler(json,nil);
     }];
}
@end
